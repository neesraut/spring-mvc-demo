<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DoCTYPE html>
<html>
<head>
<title>Student Registration Form</title>

</head>



<body>

	<form:form action="processForm" modelAttribute="student">


		<!-- modelAttribute is the modal send from controller. path calls getFirstName when load first time and calls set method when 
		submitted. hence need to set getter setter methods and property should be in class. -->
	First Name: <form:input path="firstName" />
		<br>
		<br>
	Last Name: <form:input path="lastName" />
		<br>
		<br>
	Country: 
		<form:select path="country">

			<form:options items="${student.countryOptions}"/>

		</form:select>
		<br>
		<br>
		
		Favorite Language:
		<form:radiobuttons path="favoriteLanguage" items="${student.favLangRadioOption}"/>
		<br><br>
		
		Operating Systems:
		Linux<form:checkbox path="operatingSystems" value="Linux"/>
		Ms Windows<form:checkbox path="operatingSystems" value="MS Window"/>
		Mac OS<form:checkbox path="operatingSystems" value="Mac OS"/>
		
		<br><br>

		<input type="submit" value="Submit" />

	</form:form>





</body>
</html>