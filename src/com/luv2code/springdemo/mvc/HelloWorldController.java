package com.luv2code.springdemo.mvc;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/hello") //parent mapping
public class HelloWorldController {
	
	//need a method to show initial form
	
	@RequestMapping("/showForm") //sub mappings relative i.e /hello/shpwForm
	public String showForm() {
		return "helloworld-form";
	}
	
	
	//need a method to process form
	@RequestMapping("/processForm")
	public String processForm() {
		return "helloworld";
	}
	
	//new controller method to read form data
	//add data to the model
	
	@RequestMapping("/processFormVersionTwo")
	public String letsShoutDude(HttpServletRequest request, Model model) {
		
		//read the request parameter from the html form
		
		String theName = request.getParameter("studentName");
		
		
		//procees it
		
		String result = "Yoo "+ theName;
		
		//add proceed data to the model
		model.addAttribute("message",result);
		
		
		return "helloworld";
	}
	
	
	@RequestMapping("/processFormVersionThree")
	public String processFormVersionThree(
			@RequestParam("studentName") String theName,
			Model model) {
		
		//read the request parameter from the html form
		
		
		
		
		//procees it
		theName = theName.toUpperCase();
		
		String result = "Hey my friend from v3 "+ theName;
		
		//add proceed data to the model
		model.addAttribute("message",result);
		
		
		return "helloworld";
	}
	
	
	
	

}
