package com.luv2code.springdemo.mvc.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CourseCodeConstraintValidator implements ConstraintValidator<CourseCode, String>{

	
	private String[] coursePrefix; 
	
	@Override
	public void initialize(CourseCode theCourseCode) {
		coursePrefix= theCourseCode.Value();
	}
	
	@Override //theCode is entered by the user in html form
	public boolean isValid(String theCode, ConstraintValidatorContext theConstraintValidatorContext) {
		boolean result =false; 
		/*
		 * if(theCode !=null) { result = theCode.startsWith(coursePrefix); }else {
		 * result = true; }
		 * 
		 * return result;
		 */
		if(theCode !=null) {
			for(String code : coursePrefix) {
				result = theCode.startsWith(code);
				if(result) {
					break;
				}
			}
		}else {
			result = true;
		}
		
		return result;
		
	}
	
	
}
