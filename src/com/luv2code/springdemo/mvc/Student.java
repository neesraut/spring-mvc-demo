package com.luv2code.springdemo.mvc;

import java.util.LinkedHashMap;

public class Student {
	
	private String firstName;
	private String lastName;
	private String country;
	private LinkedHashMap<String, String> countryOptions;
	private String favoriteLanguage;
	private String[] operatingSystems;
	
	public String[] getOperatingSystems() {
		return operatingSystems;
	}

	public void setOperatingSystems(String[] operatingSystems) {
		this.operatingSystems = operatingSystems;
	}

	private LinkedHashMap<String, String> favLangRadioOption;
	
	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Student() {
		countryOptions = new LinkedHashMap<String, String>();
		countryOptions.put("BR", "Brazil");
		countryOptions.put("NP", "Nepal");
		countryOptions.put("AUS", "Australia");
		countryOptions.put("IN", "India");
		countryOptions.put("PK", "Pakistan");
		
		
		//radio options value and label in hash map
		favLangRadioOption = new LinkedHashMap<String, String>();
		favLangRadioOption.put("Java","Java");
		favLangRadioOption.put("C++","C++");
		favLangRadioOption.put("PHP","PHP");
		favLangRadioOption.put("Ruby","Ruby");
		favLangRadioOption.put("Groovy","Groovy");
		
		
	}
	
	

	public LinkedHashMap<String, String> getCountryOptions() {
		return countryOptions;
	}

	
	
	public LinkedHashMap<String, String> getFavLangRadioOption() {
		return favLangRadioOption;
	}
	

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFavoriteLanguage() {
		return favoriteLanguage;
	}

	public void setFavoriteLanguage(String favoriteLanguage) {
		this.favoriteLanguage = favoriteLanguage;
	}

	
}
